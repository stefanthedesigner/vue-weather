import axios from 'axios'

export default {
  updateWeather (context, payload) {
    const lat = payload[0]
    const lng = payload[1]
    axios.get('https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/6c6630894a2a4d3b74f7189a429f4bfb/' + lat + ',' + lng + '', { crossdomain: true })
      .then(response => (context.commit('setWeatherData', response.data)))
  }
}
